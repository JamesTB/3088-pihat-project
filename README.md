# 3088 PiHAT Project

This Repository is intended for my 3088 PiHAT Project.  My PiHAT is a low-cost Sound Pressure Level (SPL) Device that is intended to have 3 modes of operation.

Modes:
1.  High sound level detection.
2.  Low sound level detection.
3.  Bounded sound level detection.

Mode 1:
Mode 1 will allow the user to detect when the volume of sound in a room is above a certain threshold.  This is useful in industrial scenarios where loud noises can damage a workers hearing.

Mode 2:
This mode triggers when the sound is below a threshold.  This allows for ensuring a quiet environemnt but unlike the previous mode, it is geared at detecting specifically low sound levels.

Mode 3:
This final mode is intended for where a specific range of volume is required and will trigger when the HAT detects a volume that starts to go outside of that range.

This briefly describes the operation of the PiHAT in this project repository.


PiHAT Breakdown:
The project has been worked on by three people.  Each working on their own subsystem within the project.
James Barnard worked on the Power Regulator portion of the PiHAT.
Asher Moncho worked on the Amplification stage and the ADC processing.
Mihlali Sifuba worked on the LED Array and output User Interface.
